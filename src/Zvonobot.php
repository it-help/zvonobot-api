<?php

namespace Zvonobot\API;


use Zvonobot\API\Scheme\RecordScheme;
use Zvonobot\API\Scheme\VoiceMailScheme;

class Zvonobot
{
    /**
     * @var string
     */
    private string $apiKey;

    /**
     * @var Http\Client
     */
    private \Zvonobot\API\Http\Client $client;

    public function __construct(string $apiKey)
    {
        $this->apiKey = $apiKey;
        $this->client = new \Zvonobot\API\Http\Client();
    }

    /**
     * @param VoiceMailScheme $voiceMail
     * @return \Zvonobot\API\Http\Response
     * @throws Scheme\Exceptions\RecordSchemeSourceException
     * @throws Scheme\Exceptions\RecordSchemeTextException
     */
    public function registerVoiceMail(VoiceMailScheme $voiceMail): \Zvonobot\API\Http\Response
    {
        $data =$voiceMail->toArray();
        $data['apiKey'] = $this->apiKey;

        return $this->client->send('create', $data);
    }

    /**
     * @param RecordScheme $record
     * @return \Zvonobot\API\Http\Response
     * @throws Scheme\Exceptions\RecordSchemeSourceException
     * @throws Scheme\Exceptions\RecordSchemeTextException
     */
    public function registerVoiceRecord(RecordScheme $record): \Zvonobot\API\Http\Response
    {
        $data = $record->toArray();
        $data['apiKey'] = $this->apiKey;
        if ($data['source'] === 'file') {
            $data['file'] = new \CURLFile($record->getFile());
        }
        return $this->client->send('createRecord', $data, false);
    }

    /**
     * @param Scheme\OutgoingPhoneScheme $outgoingPhone
     * @return \Zvonobot\API\Http\Response
     */
    public function registerOutgoingPhone(\Zvonobot\API\Scheme\OutgoingPhoneScheme $outgoingPhone): \Zvonobot\API\Http\Response
    {
        $data = $outgoingPhone->toArray();
        $data['apiKey'] = $this->apiKey;

        return $this->client->send('createPhone', $data);
    }

    /**
     * @param bool|null $all
     * @return Http\Response
     */
    public function outgoingPhones(?bool $all = false): \Zvonobot\API\Http\Response
    {
        $data = [
            'all' => $all,
            'apiKey' => $this->apiKey
        ];

        return $this->client->send('getPhones', $data);
    }



    /**
     * @param array $ids
     * @return \Zvonobot\API\Http\Response
     */
    public function removeVoiceRecords(array $ids): \Zvonobot\API\Http\Response
    {
        $data = [
            'apiKey' => $this->apiKey,
            'idList' => $ids
        ];

        return $this->client->send('removeRecord', $data);
    }
}
