<?php


namespace Zvonobot\API\Http;


class Response
{
    /**
     * @var string
     */
    private string $status;

    /**
     * @var string
     */
    private string $message;

    /**
     * @var array|null
     */
    private ?array $body;

    /**
     * Response constructor.
     * @param string $json_input
     */
    public function __construct(string $json_input)
    {
        $input = json_decode($json_input , true);

        $this->status = $input['status']??'fatal error';
        $this->message = $input['message']??(!empty($input['data']) ? $input['data']['message']??"" : "");
        $this->body = $input['data']??null;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return array|null
     */
    public function getBody(): ?array
    {
        return $this->body;
    }

}
