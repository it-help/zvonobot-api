<?php


namespace Zvonobot\API\Http;


class Client
{
    CONST API_URI = 'https://lk.zvonobot.ru/apiCalls/';

    public function send(string $path, ?array $data = [], bool $isJson = true): \Zvonobot\API\Http\Response
    {
        $response = null;
        if( $curl = curl_init() ) {
            curl_setopt($curl, CURLOPT_URL, self::API_URI . $path);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER,true);
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $isJson ? json_encode($data) : $data);
            curl_setopt($curl, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
            if ($isJson) {
                curl_setopt($curl, CURLOPT_HTTPHEADER, ['Content-Type: application/json', 'Accept: application/json']);
            }

            $out = curl_exec($curl);
            $response = new \Zvonobot\API\Http\Response($out);
            curl_close($curl);
        }

        return $response;
    }


}
