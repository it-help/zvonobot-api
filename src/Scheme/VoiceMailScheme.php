<?php


namespace Zvonobot\API\Scheme;


class VoiceMailScheme implements SchemeInterface
{
    /**
     * @var string[]
     */
    private array $phones = [];

    /**
     * @var string|null
     */
    private ?string $phone = null;

    /**
     * @var string|null
     */
    private ?string $outgoingPhone = null;

    /**
     * @var string|null
     */
    private ?string $webhookUrl = null;

    /**
     * @var \DateTimeInterface|null
     */
    private ?\DateTimeInterface $plannedAt = null;

    /**
     * @var string|null
     */
    private ?string $language = null;

    /**
     * @var bool
     */
    private bool $needRecording = false;

    /**
     * @var \Zvonobot\API\Scheme\RecordScheme|null
     */
    private ?\Zvonobot\API\Scheme\RecordScheme $record = null;

    /**
     * @var \Zvonobot\API\Scheme\IVRScheme[]
     */
    private array $ivrs = [];

    public function __construct(string $outgoingPhone)
    {
        $this->setOutgoingPhone($outgoingPhone);
    }

    /**
     * @return string[]
     */
    public function getPhones(): array
    {
        return $this->phones ?? [$this->phone];
    }

    /**
     * @param string[]|string $phones
     */
    public function setPhones($phones): void
    {
        switch (true) {
            case (is_array($phones)) :
                $this->phones = [];
                $this->phone = null;
                foreach(array_map(function (string $phone): string {
                    return preg_replace(['/\D/', '/^8/'], ['','7'], $phone);
                }, $phones) as $phone) {
                    if (!in_array($phone, $this->phones)) {
                        $this->phones[] = $phone;
                    }
                }

                $this->phone = null;
            break;
            case (is_string($phones)) :
                $this->phone = preg_replace(['/\D/', '/^8/'], ['','7'], $phones);
                $this->phones = [];
            break;
        }
    }

    /**
     * @param string $phone
     * @return $this
     */
    public function addPhone(string $phone): self
    {
        $phone = preg_replace(['/\D/', '/^8/'], ['','7'], $phone);

        switch (true) {
            case (empty($this->phones) && empty($this->phone)):
                $this->phone = $phone;
                break;
            case (!empty($this->phone)):
                if ($this->phone !== $phone) {
                    $this->phones[] = $this->phone;
                    $this->phones[] = $phone;
                    $this->phone = null;
                }
                break;
            default:
                if (!in_array($phone, $this->phones)) {
                    $this->phones[] = $phone;
                }
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getOutgoingPhone(): ?string
    {
        return $this->outgoingPhone;
    }

    /**
     * @param string|null $outgoingPhone
     */
    public function setOutgoingPhone(?string $outgoingPhone): void
    {
        $this->outgoingPhone = preg_replace(['/\D/', '/^8/'], ['','7'], $outgoingPhone);
    }

    /**
     * @return string|null
     */
    public function getWebhookUrl(): ?string
    {
        return $this->webhookUrl;
    }

    /**
     * @param string|null $webhookUrl
     */
    public function setWebhookUrl(?string $webhookUrl): void
    {
        $this->webhookUrl = $webhookUrl;
    }

    /**
     * @return RecordScheme
     */
    public function getRecord(): RecordScheme
    {
        return $this->record;
    }

    /**
     * @param RecordScheme $record
     */
    public function setRecord(RecordScheme $record): void
    {
        $this->record = $record;
    }

    /**
     * @return IVRScheme[]
     */
    public function getIvrs(): array
    {
        return $this->ivrs;
    }

    /**
     * @param IVRScheme $ivr
     */
    public function addIVR(IVRScheme $ivr): void
    {
        $this->ivrs[] = $ivr;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getPlannedAt(): ?\DateTimeInterface
    {
        return $this->plannedAt;
    }

    /**
     * @param \DateTimeInterface|null $plannedAt
     */
    public function setPlannedAt(?\DateTimeInterface $plannedAt): void
    {
        $this->plannedAt = $plannedAt;
    }

    /**
     * @return string|null
     */
    public function getLanguage(): ?string
    {
        return $this->language;
    }

    /**
     * @param string $language
     */
    public function setLanguage(string $language): void
    {
        $this->language = $language;
    }

    /**
     * @return bool
     */
    public function isNeedRecording(): bool
    {
        return $this->needRecording;
    }

    /**
     * @param bool|null $needRecording
     */
    public function setNeedRecording(?bool $needRecording): void
    {
        $this->needRecording = !empty($needRecording);
    }

    /**
     * @return array
     * @throws Exceptions\RecordSchemeSourceException
     * @throws Exceptions\RecordSchemeTextException
     */
    public function toArray(): array
    {
        return array_merge(
            [
                'outgoingPhone' => $this->outgoingPhone,
                'recognize' => (int) !empty(array_filter($this->ivrs, function ($ivr) { return !empty($ivr->getKeyWords()); } )),
                'record' => $this->record->toArray(),
                'ivrs' => array_map(function ($ivr) { return $ivr->toArray(); }, $this->ivrs)
            ],
            !empty($this->plannedAt) ? [ 'plannedAt' => $this->plannedAt->format('U') ] : [],
            !empty($this->language) ? [ 'lang' => $this->language ] : [],
            !empty($this->needRecording) ? [ 'needRecording' => (int) $this->needRecording ] : [],
            empty($this->phones) ? [ 'phone'=>$this->phone ] : ['phones' => $this->phones ],
            empty($this->webhookUrl) ? [] : [ 'webhookUrl' => $this->webhookUrl ],
        );
    }
}