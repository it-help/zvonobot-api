<?php


namespace Zvonobot\API\Scheme\Exceptions;


class RecordSchemeTextException extends \Exception
{
    public function __construct()
    {
        parent::__construct("There is no message to generate for record type \"text\".");
    }
}