<?php

namespace Zvonobot\API\Scheme\Exceptions;

class RecordSchemeSourceException extends \Exception
{
    public function __construct()
    {
        parent::__construct('Unknown recording source type.');
    }
}
