<?php


namespace Zvonobot\API\Scheme;

use Zvonobot\API\Scheme\IVRWebhookScheme;

class IVRScheme implements SchemeInterface
{
    /**
     * @var string|null
     */
    private ?string $digit = null;

    /**
     * @var string[]
     */
    private array $keyWords = [];

    /**
     * @var bool
     */
    private bool $anyWord = false;

    /**
     * @var bool
     */
    private bool $needBlock = false;

    /**
     * @var string|null
     */
    private ?string $managerPhone = null;

    /**
     * @var string|null
     */
    private ?string $smsText = null;

    /**
     * @var IVRScheme[]
     */
    private array $ivrs = [];

    /**
     * @var IVRWebhookScheme|null
     */
    private ?IVRWebhookScheme $webhook = null;

    /**
     * @var RecordScheme|null
     */
    private ?\Zvonobot\API\Scheme\RecordScheme $record = null;

    /**
     * @return string|null
     */
    public function getDigit(): ?string
    {
        return $this->digit;
    }

    /**
     * @param string|null $digit
     */
    public function setDigit(?string $digit): void
    {
        $this->digit = preg_match('/^([0-9\#\*])(.*)$/', $digit, $matches) ? $matches[1]??null : null;
    }

    /**
     * @return string[]
     */
    public function getKeyWords(): array
    {
        return $this->keyWords;
    }

    /**
     * @param string[] $keyWords
     */
    public function setKeyWords(array $keyWords): void
    {
        $this->keyWords = $keyWords;
    }

    /**
     * @return bool
     */
    public function isAnyWord(): bool
    {
        return $this->anyWord;
    }

    /**
     * @param bool $anyWord
     */
    public function setAnyWord(bool $anyWord): void
    {
        $this->anyWord = $anyWord;
    }

    /**
     * @return bool
     */
    public function isNeedBlock(): bool
    {
        return $this->needBlock;
    }

    /**
     * @param bool $needBlock
     */
    public function setNeedBlock(bool $needBlock): void
    {
        $this->needBlock = $needBlock;
    }

    /**
     * @return string|null
     */
    public function getManagerPhone(): ?string
    {
        return $this->managerPhone;
    }

    /**
     * @param string|null $managerPhone
     */
    public function setManagerPhone(?string $managerPhone): void
    {
        $this->managerPhone = $managerPhone;
    }

    /**
     * @return string|null
     */
    public function getSmsText(): ?string
    {
        return $this->smsText;
    }

    /**
     * @param string|null $smsText
     */
    public function setSmsText(?string $smsText): void
    {
        $this->smsText = $smsText;
    }

    /**
     * @return IVRScheme[]
     */
    public function getIvrs(): array
    {
        return $this->ivrs;
    }

    /**
     * @param IVRScheme[] $ivrs
     */
    public function setIvrs(array $ivrs): void
    {
        $this->ivrs = $ivrs;
    }

    /**
     * @param IVRScheme $ivr
     * @return $this
     */
    public function addIVR(IVRScheme &$ivr): IVRScheme
    {
        $this->ivrs[] = $ivr;
        return $this;
    }

    /**
     * @return IVRWebhookScheme|null
     */
    public function getWebhook(): ?IVRWebhookScheme
    {
        return $this->webhook;
    }

    /**
     * @param IVRWebhookScheme|null $webhook
     */
    public function setWebhook(?IVRWebhookScheme $webhook): void
    {
        $this->webhook = $webhook;
    }

    /**
     * @return RecordScheme|null
     */
    public function getRecord(): ?RecordScheme
    {
        return $this->record;
    }

    /**
     * @param RecordScheme $record
     */
    public function setRecord(RecordScheme $record): void
    {
        $this->record = $record;
    }

    public function isRecognize(): bool
    {
        return !empty(array_filter($this->ivrs, function (IVRScheme $ivr) {
            return $ivr->isAnyWord() || !empty($ivr->getKeyWords());
        }));
    }

    /**
     * @return array
     * @throws Exceptions\RecordSchemeSourceException
     * @throws Exceptions\RecordSchemeTextException
     */
    public function toArray(): array
    {
        $data = [];

        switch (true) {
            case ($this->anyWord) : $data = array_merge($data, [ 'anyWord' => true ]); break;
            case ($this->digit !== null): $data = array_merge($data, [ 'digit' => $this->digit ]); break;
            case (!empty($this->keyWords)) : $data = array_merge($data, [ 'keyWords' => implode("|",
                array_map(function($keyword) { return trim($keyword); }, $this->keyWords)) ]); break;
            default: $data = array_merge($data, [ 'anyDigit' => 1 ]);
        }

        return array_merge(
            $data,
            $this->smsText ? [ 'smsText' => $this->smsText ] : [],
            $this->webhook ? $this->webhook->toArray() : [],
            $this->record ? ['record' => $this->record->toArray()] : [],
            !empty($this->managerPhone) ? ['managerPhone' => $this->managerPhone] : [],
            [
                'recognize' => (int) $this->isRecognize(),
                'needBlock' => $this->needBlock ? (int) $this->needBlock : 0,
                'ivrs' => !empty($this->ivrs) ? array_map(function($ivr) { return $ivr->toArray(); }, $this->ivrs) : []
            ]
        );
    }
}
