<?php


namespace Zvonobot\API\Scheme;


use Zvonobot\API\Scheme\Exceptions\RecordSchemeSourceException;
use Zvonobot\API\Scheme\Exceptions\RecordSchemeTextException;

class RecordScheme implements SchemeInterface
{
    public const SOURCE_TYPES_TEXT = 'text';
    public const SOURCE_TYPES_AUDIO = 'file';

    public const GENDER_MAN = 1;
    public const GENDER_WOMAN = 0;

    /**
     * @var int|null
     */
    private ?int $externalId = null;

    /**
     * @var string|null
     */
    private ?string $name = null;

    /**
     * @var ?string
     */
    private ?string $source = null;

    /**
     * @var int
     */
    private int $gender = self::GENDER_WOMAN;

    /**
     * @var string|null
     */
    private ?string $text = null;

    /**
     * @var mixed|null
     */
    private $file = null;

    /**
     * @return int|null
     */
    public function getExternalId(): ?int
    {
        return $this->externalId;
    }

    /**
     * @param int|null $externalId
     */
    public function setExternalId(?int $externalId): void
    {
        $this->externalId = $externalId;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSource(): string
    {
        return $this->source;
    }

    /**
     * @return int
     */
    public function getGender(): int
    {
        return $this->gender;
    }

    /**
     * @param int $gender
     */
    public function setGender(int $gender): void
    {
        $this->gender = $gender;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     */
    public function setText(?string $text): void
    {
        !empty($this->text = $text) ? $this->source = self::SOURCE_TYPES_TEXT : null;
    }

    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param mixed $file
     */
    public function setFile($file): void
    {
        if (!empty($this->file = $file)) {
            $this->source = self::SOURCE_TYPES_AUDIO;
        }
    }

    /**
     * @return string[]
     * @throws RecordSchemeSourceException
     * @throws RecordSchemeTextException
     */
    public function toArray(): array
    {
        if ($this->externalId) {
            return [ 'id' => $this->externalId ];
        } else if ($this->source) {
            $data = array_merge(
                $this->name ? ['name' => $this->name] : [],
                [ 'source' => $this->source ]);

            switch ($this->source) {
                case 'text' :
                    if (empty($this->text)) {
                        throw new RecordSchemeTextException();
                    }
                    return array_merge($data, [ 'text' => $this->text, 'gender' => $this->gender ]);
                case 'file' :
                    return array_merge($data, [ 'file' => $this->file ]);
                default:
                    throw new RecordSchemeSourceException();
            }
        }

        return [];
    }


}
