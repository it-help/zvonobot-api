<?php


namespace Zvonobot\API\Scheme;


class IVRWebhookScheme implements SchemeInterface
{
    /**
     * @var string
     */
    private string $webhookUrl;

    /**
     * @var array
     */
    private array $webhookParameters = [];

    /**
     * @return string
     */
    public function getWebhookUrl(): string
    {
        return $this->webhookUrl;
    }

    /**
     * @param string $webhookUrl
     */
    public function setWebhookUrl(string $webhookUrl): void
    {
        $this->webhookUrl = $webhookUrl;
    }

    /**
     * @return array|null
     */
    public function getWebhookParameters(): ?array
    {
        return $this->webhookParameters;
    }

    /**
     * @param array $webhookParameters
     */
    public function setWebhookParameters(array $webhookParameters): void
    {
        $this->webhookParameters = $webhookParameters;
    }

    /**
     * @param string $parameterName
     * @param $parameterValue
     * @return IVRWebhookScheme
     */
    public function addWebhookParameter(string $parameterName, $parameterValue): self
    {
        $this->webhookParameters[$parameterName] = $parameterValue??null;

        return $this;
    }

    /**
     * IVRWebhookScheme constructor.
     * @param string $webhookUrl
     * @param array|null $webhookParameters
     */
    public function __construct(string $webhookUrl, ?array $webhookParameters = [])
    {
        $this->webhookUrl = $webhookUrl;
        $this->webhookParameters = $webhookParameters??[];
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'webhookUrl' => $this->webhookUrl,
            'webhookParameters' => json_encode($this->webhookParameters)
        ];
    }
}