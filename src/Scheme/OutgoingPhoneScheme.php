<?php


namespace Zvonobot\API\Scheme;


class OutgoingPhoneScheme implements SchemeInterface
{
    /**
     * @var string
     */
    private string $outgoingPhone;

    /**
     * @return string
     */
    public function getOutgoingPhone(): string
    {
        return $this->outgoingPhone;
    }

    /**
     * @param string $outgoingPhone
     */
    public function setOutgoingPhone(string $outgoingPhone): void
    {
        $this->outgoingPhone = $outgoingPhone;
    }

    /**
     * @return string[]
     */
    public function toArray(): array
    {
        return [
            'phone' => $this->outgoingPhone
        ];
    }

}