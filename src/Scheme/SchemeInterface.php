<?php


namespace Zvonobot\API\Scheme;


interface SchemeInterface
{
    /**
     * @return array
     */
    public function toArray(): array;

}