<?php


namespace Scheme;


use PHPUnit\Framework\TestResult;
use Zvonobot\API\Scheme\RecordScheme;

class VoiceMailSchemeTest extends \PHPUnit\Framework\TestCase
{

    private ?\Zvonobot\API\Scheme\VoiceMailScheme $voiceMail;

    protected function setUp(): void
    {
        $this->voiceMail = new \Zvonobot\API\Scheme\VoiceMailScheme('+71112221111');
        $this->voiceMail
            ->addPhone('+7(111) 111-11-11')
            ->addPhone('8(111) 11111 11')
            ->addPhone('71111111111');

        $plannedAt = new \DateTime();
        $plannedAt->modify('+1days');
        $this->voiceMail->setPlannedAt($plannedAt);

        $this->voiceMail->setNeedRecording(true);

        $record = new RecordScheme();
        $record->setText('Unit test!');

        $this->voiceMail->setRecord($record);
    }


    public function testToArray()
    {
        $this->assertSame($this->voiceMail->toArray(), [
            'outgoingPhone' => $this->voiceMail->getOutgoingPhone(),
            'recognize' => 0,
            'record' => [
                'source' => $this->voiceMail->getRecord()->getSource(),
                'text' => $this->voiceMail->getRecord()->getText(),
                'gender' => $this->voiceMail->getRecord()->getGender()
            ],
            'ivrs' => [],
            'plannedAt' => $this->voiceMail->getPlannedAt()->format('U'),
            'needRecording' => (int) $this->voiceMail->isNeedRecording(),
            'phone' => '71111111111'
        ]);
    }
}