<?php


namespace Zvonobot\Tests\API\Scheme;


use PHPUnit\Framework\TestCase;


class IVRWebhookSchemeTest extends TestCase
{
    private ?\Zvonobot\API\Scheme\IVRWebhookScheme $ivrWebhook;


    protected function setUp(): void
    {
        $this->ivrWebhook = new \Zvonobot\API\Scheme\IVRWebhookScheme('https://your.domain.zone/path/to/status/callback');
        $this->ivrWebhook->setWebhookParameters([
            'client' => '{phone}',
            'action' => 'ActionName',
            'parameter1' => 'value1',
            'parameter2' => 'value2'
        ]);
    }

    protected function tearDown(): void
    {
        $this->ivrWebhook = null;
    }

    public function testToArray()
    {
        $testedArray = $this->ivrWebhook->toArray();

        $this->assertSame($testedArray, [
            'webhookUrl' => 'https://your.domain.zone/path/to/status/callback',
            'webhookParameters' =>'{"client":"{phone}","action":"ActionName","parameter1":"value1","parameter2":"value2"}'
        ]);
    }
}